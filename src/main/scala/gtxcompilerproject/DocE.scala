package gtxcompilerproject
/**
  * A class to indicate the end of document and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */
case class DocE() {
  override def toString : String = "</html>"
}
