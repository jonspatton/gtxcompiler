/*

DEPRICATED: Used in the package object instead.


package gtxcompilerproject

/**
  * Lists of special characters and "termal-ish" tags for convenience.
  */
package object CONSTANTS {
  /**
    * The list of special characters and tags that denote the start of a tag or annotated block
    */

  //Single-character tags (note these are actual chars. They are eventually mapped to strings in TERMINALS)
  val ADDRESSB  = '('
  val ADDRESSE  = ')'
  val BRACKETE  = ']'
  val BOLD      = '*'
  val IMAGEB    = '!'
  val LISTITEM  = '+'
  val LINKB     = '['
  val EQSIGN    = '='
  val HEADING   = '#'

  //Slash tags
  val DOCB      = "\\BEGIN"
  val DOCE      = "\\END"
  val NEWLINE   = "\\\\"
  val PARAB     = "\\PARAB"
  val PARAE     = "\\PARAE"
  val TITLEB    = "\\TITLE["
  val DEFB      = "\\DEF["
  val USEB      = "\\USE["

  /** Text characters (note these are actual chars. They are eventually mapped to strings in TERMINALS).
    */

  val UPPERCASE = List('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z')
  val LOWERCASE = List('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z')
  val ALPHA     = UPPERCASE ::: LOWERCASE
  val NUMERIC   = List('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
  val OTHER     = List('.', ':', ',', '?', '_', '/', '"')
  val SPACE     = ' '
  val TAB       = '\t'
  val CARRIAGERETURN = List('\t', '\n', '\r', '\u000C')
  val WHITESPACE = List(SPACE, TAB) ::: CARRIAGERETURN


  val REQTEXT   = ALPHA ::: NUMERIC ::: OTHER ::: List(SPACE, TAB)
  val TEXT      = REQTEXT ::: WHITESPACE ::: List(NEWLINE)
  val SPECIAL   = List(ADDRESSB, ADDRESSE, BRACKETE, BOLD, EQSIGN, HEADING, IMAGEB, LISTITEM, LINKB) ::: WHITESPACE
  val TAGS      = List(DOCB, DOCE, NEWLINE, PARAB, PARAE, TITLEB, DEFB, USEB) ::: SPECIAL

  /**
    * Everything. Every entry is mapped to a string so that the lexical analyzer only ever has to check one place
    * for a token's legality.
    */
  val TERMINAL = (TAGS ::: TEXT).map(_.toString)


}
*/