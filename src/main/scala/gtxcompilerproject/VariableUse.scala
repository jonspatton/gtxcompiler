package gtxcompilerproject

/**
  * Keeps track of the name of the variable referred to in the variable use.
  * At compile time, this object is replaced by a (string) object whose value is
  * stored in the corresponding VariableDefine with the same name.
  *
  * Author: Jon S. Patton
  */
case class VariableUse() {
  var name = ""

  /**
    * @return The variable name to be used.
    */
  def getName() = name

  /**
    * @param s The new variable name to be used.
    */
  def setName(s : String) = name = s

  override def toString() : String = name

}
