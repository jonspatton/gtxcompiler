package gtxcompilerproject

/**
  * Lexical analyzer trait to force specific method names on the project (to aid troubleshooting).
  *
  * Author: J. Dehlinger
  */

trait LexicalAnalyzer {
  def addChar(c : Char, s: String) : String
  def getChar() : Char
  def getNextToken() : String
  def lookup(candidate : String) : Boolean
}
