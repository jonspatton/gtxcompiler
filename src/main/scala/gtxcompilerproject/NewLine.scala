package gtxcompilerproject

/**
  * A class to report the relevant HTML code for a line break.
  *
  * Author: Jon S. Patton
  */

case class NewLine() {

  override def toString() : String = "<br>"
}
