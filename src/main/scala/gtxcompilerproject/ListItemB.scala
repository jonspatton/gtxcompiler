package gtxcompilerproject

/**
  * A class to indicate the start of a list item and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

case class ListItemB() {

  override def toString() : String = "<li>"

}
