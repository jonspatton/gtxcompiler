package gtxcompilerproject

/**
  * Stores the name and value of a variable definition.
  * At compilation time, this object will report the value to replace the VariableUse object
  * with a matching name.
  *
  * Author: Jon S. Patton
  */
case class VariableDefine() {
  var value = ""
  var name = ""

  /**
    * @return The variable's name.
    */
  def getName() : String = name

  /**
    * @return The variable's value.
    */
  def getValue() : String = value

  /**
    * @param s The new variable name.
    */
  def setName(s : String) = name = s

  /**
    * @param s The new variable value.
    */
  def setValue(s : String) = value = s

  override def toString() : String = name + " = " + value
}