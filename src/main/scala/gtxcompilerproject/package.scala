/**
  * An object to store the constants and global variables and global error function.
  *
  * Author: Jon S. Patton
  *
  * General citations.
  *
  * 1. Scala cheatsheet. https://docs.scala-lang.org/cheatsheets/
  * 2. Scala documentation. https://www.scala-lang.org/api/2.9.1/scala/package.html
  * 3. Case classes and some weirder pattern matching: https://docs.scala-lang.org/tour/pattern-matching.html
  * 4. HTML cheatsheet http://www.simplehtmlguide.com/cheatsheet.php
*/


package object gtxcompilerproject {

  /**    Global Line Count    */
  var LINECOUNT : Int = 1

  /** Global Position Count    */
  var POSITION : Int  = 1

  /* ***************************************************************************************************************
    * The list of special characters and tags that denote the start of a tag or annotated block
    *************************************************************************************************************** */

  /* ***************************************************************************************************************
    * Single-character tags (note these are actual chars. They are eventually mapped to strings in TERMINALS)
    *************************************************************************************************************** */

  val ADDRESSB  = "("
  val ADDRESSE  = ")"
  val BRACKETE  = "]"
  val BOLD      = "*"
  val IMAGEB    = "!["
  val LISTITEM  = "+"
  val LINKB     = "["
  val EQSIGN    = "="
  val HEADING   = "#"

  //Slash tags
  val DOCB      = "\\BEGIN"
  val DOCE      = "\\END"
  val NEWLINE   = "\\\\"
  val PARAB     = "\\PARAB"
  val PARAE     = "\\PARAE"
  val TITLEB    = "\\TITLE["
  val DEFB      = "\\DEF["
  val USEB      = "\\USE["

  /* ***************************************************************************************************************
  * Text characters (note these are actual chars. They are eventually mapped to strings in TERMINALS).
    ***************************************************************************************************************  */

  val UPPERCASE = List("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")
  val LOWERCASE = List("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z")
  val ALPHA     = UPPERCASE ::: LOWERCASE
  val NUMERIC   = List("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")
  val OTHER     = List(".", ":", ",", "?", "_", "/", "\"")
  val SPACE     = " "
  val TAB       = "\t"
  val CARRIAGERETURN = List("\n", "\r", "\u000C")
  val WHITESPACE = List(SPACE, TAB) ::: CARRIAGERETURN


  val REQTEXT   = ALPHA ::: NUMERIC ::: OTHER ::: List(SPACE, TAB)
  val TEXT      = REQTEXT ::: WHITESPACE
  val SPECIAL   = List(ADDRESSB, ADDRESSE, BRACKETE, BOLD, EQSIGN, HEADING, IMAGEB, LISTITEM, LINKB)
  val TAGS      = List(DOCB, DOCE, NEWLINE, PARAB, PARAE, TITLEB, DEFB, USEB) ::: SPECIAL

  val INNERITEM = List(BOLD, LINKB, USEB) ::: REQTEXT
  val INNERTEXT = List(HEADING, LISTITEM, IMAGEB) ::: INNERITEM ::: WHITESPACE
  val BODYTAGS  = PARAB :: INNERTEXT

  /**
    * Everything: Every entry is mapped to a string so that the lexical analyzer only ever has to check one place
    * for a token's legality.
    */
  val TERMINAL  = (TAGS ::: TEXT)

  /** Legal distinct characters. */
  val LEGALCHARACTERS : List[Char]  = TERMINAL.flatten.distinct
  /** Longest possible legal token. */
  val MAXTOKENLENGTH  = TERMINAL.maxBy(_.length).length


  /* ***************************************************************************************************************
    * Global error state and generic error function.
    *************************************************************************************************************** */

  /** Global error state. */
  var NOERROR : Boolean = true

  /** Appends an error message to the error tree (to be printed out, if not empty, at the end of compilation). */
  def error(msg : String): Unit = {
    ErrorTree.add(msg)
    NOERROR = true
  }
}
