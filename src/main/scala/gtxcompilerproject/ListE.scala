package gtxcompilerproject

/**
  * A class to indicate the end of an unordered list and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

case class ListE() {

  override def toString() = "</ul>\n"

}
