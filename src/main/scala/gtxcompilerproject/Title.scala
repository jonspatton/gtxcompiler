package gtxcompilerproject

/**
  * A class to store the title of the HTML document and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

case class Title() {
  var text : String = ""

  /**
    * @return The title text.
    */
  def get() = text

  /**
    * @param s The new title text.
    */
  def set(s: String) = text = s

  /**
    * @param s The string to add to the title text.
    */
  def add(s : String) = text = text +  s

  override def toString() : String = "<title>" + text + "</title>"

}
