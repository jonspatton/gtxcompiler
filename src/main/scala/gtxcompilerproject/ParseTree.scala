package gtxcompilerproject

/**
  * An object to store the parse tree, containing all legal tokens for the eventual output.
  *
  * Author: Jon S. Patton
  */

object ParseTree {

  var parseTree : List[Any] = List()

  def add(s : Any): Unit =
  {
    parseTree = s :: parseTree
  }
  def deleteLastAddition(): Unit =
  {
    parseTree = parseTree.tail
  }
  def getReverse(): List[Any] = {
    return parseTree.reverse
  }

  def get() : List[Any] = return parseTree

}
