package gtxcompilerproject

/**
  * A class to indicate the start of a bold tag and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

case class BoldB() {

  override def toString() : String = "<b>"

}
