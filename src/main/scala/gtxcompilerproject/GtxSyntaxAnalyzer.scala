package gtxcompilerproject

/**
  * Analyzes the syntax of each token, or series of tokens, according to the grammar rules of the GitTex language.
  *
  * All methods return true if the Syntax is correct, false if the syntax is incorrect.
  *
  * author: @Jon S. Patton
  */

class GtxSyntaxAnalyzer (scanner: GtxLexicalAnalyzer) extends SyntaxAnalyzer {

  /* ******************************************************************************************************************
  Document block functions without recursive calls.
  ****************************************************************************************************************** */

  /**
    * The syntax rule for an entire file:
    *
    * gittex::= DOCB variable-define title body DOCE
    *
    * @return error state (true for no errors)
    */
  def gittex() : Boolean = {

    clearWhiteSpace()
    //1. Gittex start tag
    NOERROR = scanner.getNextToken().equalsIgnoreCase(DOCB)
    if (NOERROR) {
      ParseTree.add(new DocB)
      scanner.getNextToken()
    }
    else {
      standardSyntaxError(DOCB)
    }

    //Clear optional white space
    clearWhiteSpace()

    while (CurrentToken.get().equalsIgnoreCase(DEFB)) {
      NOERROR = variableDefine()
      clearWhiteSpace()
    }

    //Clear optional white space
    clearWhiteSpace()

    //2. Title.
    title()

    //Clear optional white space
    clearWhiteSpace()

    body()

    //clear out optional white space.
    clearWhiteSpace()

    //4. Gittex End tag
    NOERROR = CurrentToken.get().equalsIgnoreCase(DOCE)
    if (NOERROR) {
      ParseTree.add(new DocE)
    }
    else {
      standardSyntaxError(DOCE)
    }

    scanner.getNextToken()
    //Clear out the end of the file, and report an error if there are any annotations found.
    while (!CurrentToken.get().isEmpty()) {
      clearWhiteSpace()
      if (TAGS.contains(CurrentToken.get())) {
        error("Syntax Error: Annotations such as " + CurrentToken.get() + " not permitted after \\END tag")
      }
      scanner.getNextToken()
    }

    return NOERROR

  }

  /**
    * The syntax rule for title ::= TITLEB REQTEXT BRACKETE
    *
    * Since the title is a required block for the document, this function will return false early if the title
    * begin tag is simply missing.
    *
    * @return error state (true for no errors)
    */
  def title() : Boolean = {
    //Check for the title start tag
    NOERROR = CurrentToken.get().equalsIgnoreCase(TITLEB)
    var t : Title = new Title()
    if (NOERROR) {
      scanner.getNextToken()
    }
    else {
      standardSyntaxError(TITLEB)
      return false
    }

    t.set(getTextUntilStopCondition(REQTEXT, List(BRACKETE)))
    if (t.get().isEmpty()) {
      standardSyntaxError("Title text")
    }

    NOERROR = CurrentToken.get().equalsIgnoreCase(BRACKETE)
    //Add the title to the parse tree and move past the bracket end tag.
    if (NOERROR) {
      ParseTree.add(t)
      scanner.getNextToken()
    }
    else {
      standardSyntaxError(BRACKETE)
    }

    return NOERROR
  }


  /**
    * The syntax rule for paragraph ::= PARAB variable-define inner-text PARAE
    * @return error state (true for no errors)
    */
  def paragraph() : Boolean = {
    if (!CurrentToken.get().equalsIgnoreCase(PARAB)){
      return false
    }
    ParseTree.add(new ParaB)
    scanner.getNextToken()
    clearWhiteSpace()

    while (CurrentToken.get().equalsIgnoreCase(DEFB)) {
      NOERROR = variableDefine()
      clearWhiteSpace()
    }

    //InnerText
    innerText()

    //Add the end tag to the tree and advance past it.
    NOERROR = CurrentToken.get().equalsIgnoreCase(PARAE)
    if (NOERROR) {
      ParseTree.add(new ParaE)
      scanner.getNextToken()
    }
    else {
      standardSyntaxError(PARAE)
    }

    return NOERROR
  }

  /**
    * Syntax rule for
    *
    * heading ::=   HEADING REQTEXT |
    *               ε
    * @return error state (true for no errors)
    */
  def heading() : Boolean = {
    if (!CurrentToken.get().equals(HEADING)){
      return false
    }

    ParseTree.add(new HeadingB)
    scanner.getNextToken()
    addTextUntilStopCondition(REQTEXT, CARRIAGERETURN)

    //We stopped on a hard return, so advance past it.
    if (NOERROR) {
      ParseTree.add(new HeadingE)
      ParseTree.add(CurrentToken.get())
      scanner.getNextToken()
    }
    else {
      standardSyntaxError("Heading text")
      scanner.getNextToken()
    }

    return NOERROR
  }


  /**
    * Syntax rule for
    *
    * bold ::=  BOLD TEXT BOLD |
    *           ε
    * @return error state (true for no errors)
    */
  def bold() : Boolean = {
    if (!CurrentToken.get().equalsIgnoreCase(BOLD)) {
        return false
      }
    ParseTree.add(new BoldB)
    scanner.getNextToken()

    addTextUntilStopCondition(TEXT, List(BOLD))

    NOERROR = CurrentToken.get().equals(BOLD)
    //we stopped on a bold tag, so advance past it.
    if (NOERROR) {
      ParseTree.add(new BoldE)
      scanner.getNextToken()
    }
    else {
      standardSyntaxError(BOLD)
    }

    return NOERROR
  }

  /**
    * Syntax rule for
    *
    * listitem ::=  LISTITEMB inner-item list-item |
    *               ε
    * @return error state (true for no errors)
    */
  def listItem() : Boolean = {
    if (!CurrentToken.get().equals(LISTITEM)){
      return NOERROR
    }

    ParseTree.add(new ListB())

    while(CurrentToken.get().equalsIgnoreCase(LISTITEM)){
      ParseTree.add(new ListItemB())
      scanner.getNextToken()
      innerItem()
      if (CARRIAGERETURN.contains(CurrentToken.get())) {
        ParseTree.add(new ListItemE())
        ParseTree.add(new ListItemE())
        clearWhiteSpace()
      }
    }

    ParseTree.add(new ListE())
    return NOERROR
  }

  /**
    * Syntax rule for a
    *
    * link ::=LINKB REQTEXT BRACKETE ADDRESSB REQTEXT ADDRESSE | ε
    *
    * @return error state (true for no errors)
    */
  def link() : Boolean = {
    if (!CurrentToken.get().equalsIgnoreCase(LINKB)){
      return false
    }
    var li : Link = new Link()

    //Advance past the linkb tag
    scanner.getNextToken()

    //Get the link text
    li.addToText(getTextUntilStopCondition(REQTEXT, List(BRACKETE)))

    scanner.getNextToken()
    clearWhiteSpace()

    //Advance past the addressb tag (we stopped on a brackete, we get the next token and check it's an addressb)
    NOERROR = CurrentToken.get().equals(ADDRESSB)
    if (NOERROR) {
      scanner.getNextToken()
      li.addToAddress(getTextUntilStopCondition(REQTEXT, List(ADDRESSE)))
    }
    else {
      standardSyntaxError(ADDRESSB)
    }

    //Advance past the addresse tag
    if (CurrentToken.get().equals(ADDRESSE)) {
      scanner.getNextToken()
    }
    else {
      standardSyntaxError(ADDRESSE)
    }

    if (NOERROR) {
      ParseTree.add(li)
    }

    return NOERROR
  }

  /**
    * Syntax rule for the image tag and its accompanying link text.
    *
    * image ::= IMAGEB REQTEXT BRACKETE ADDRESSB REQTEXT ADDRESSE |
    *           ε
    * @return error state (true for no errors)
    */
  def image() : Boolean = {
    if (!CurrentToken.get().equalsIgnoreCase(IMAGEB)){
      return false
    }
    var img : Image = new Image()

    //Advance past the linkb tag
    scanner.getNextToken()
    img.addToAlt(getTextUntilStopCondition(REQTEXT, List(BRACKETE)))

    scanner.getNextToken()
    clearWhiteSpace()

    //Advance past the addressb tag (we stopped on a brackete, we get the next token and check it's an addressb)
    NOERROR = CurrentToken.get().equals(ADDRESSB)
    if (NOERROR) {
      scanner.getNextToken()
      img.addToAddress(getTextUntilStopCondition(REQTEXT, List(ADDRESSE)))
    }
    else {
      standardSyntaxError(ADDRESSB)
    }

    //Advance past the addresse tag, or generate an error message if it's missing.
    if (CurrentToken.get().equals(ADDRESSE)) {
      scanner.getNextToken()
    }
    else {
      standardSyntaxError(ADDRESSE)
    }

    if (NOERROR) {
      ParseTree.add(img)
    }

    return NOERROR
  }

  /**
    * Syntax rule for the new line gtx code //
    * newline> ::= NEWLINE | ε
    * @return error state (true for no errors)
    */
  def newline() : Boolean = {
    if (!CurrentToken.get().equalsIgnoreCase(NEWLINE)){
      return false
    }
    ParseTree.add(new NewLine)
    scanner.getNextToken()
    return NOERROR
  }

  /* ******************************************************************************************************************
  Variable syntax functions.
  ****************************************************************************************************************** */

  /**
    * Syntax rule for
    *
    * variable-define::=  DEFB REQTEXT EQSIGN REQTEXT BRACKETE variable-define |
    *                     ε
    * @return error state (true for no errors)
    */
  def variableDefine() : Boolean = {
    if(!CurrentToken.get().equalsIgnoreCase(DEFB)){
      return false
    }
    var d : VariableDefine = new VariableDefine()
    scanner.getNextToken()

    NOERROR = REQTEXT.contains(CurrentToken.get())

    if (NOERROR) {
      var name = getTextUntilStopCondition(REQTEXT, List(EQSIGN))
        //filter any spaces from the variable name
        .filterNot(_.isWhitespace)

      if (!name.isEmpty){
        d.setName(name)
      }
      else {
        standardSyntaxError("Variable name ")
      }
    }

    //Verify it contains an equals sign
    NOERROR = CurrentToken.get().equals(EQSIGN)

    if (NOERROR) {
      scanner.getNextToken()
    }
    else {
      standardSyntaxError(EQSIGN)
    }

    //Verify it contains a value.
    NOERROR = REQTEXT.contains(CurrentToken.get())

    if (NOERROR){
      var value = getTextUntilStopCondition(REQTEXT, List(BRACKETE))
        //filter any spaces from the definition
        .filterNot(_.isWhitespace)
      if (!value.isEmpty()){
        d.setValue(value)
      }
      else {
        standardSyntaxError("Variable value")
      }

    }
    else {
      standardSyntaxError("Variable definition value")
    }

    NOERROR = CurrentToken.get().equalsIgnoreCase(BRACKETE)

    if (NOERROR) {
      ParseTree.add(d)
      //We ended on a bracket, so move past it.
      scanner.getNextToken()
    }
    else {
      standardSyntaxError(BRACKETE)
    }

    return NOERROR
  }

  /**
    * Syntax rule for
    *
    * variable-use ::=  USEB REQTEXT BRACKETE |
    *                   ε
    * @return error state (true for no errors)
    */
  def variableUse() : Boolean = {
    if (!CurrentToken.get().equalsIgnoreCase(USEB)){
      return false
    }
    var u : VariableUse = new VariableUse()
    scanner.getNextToken()

    NOERROR = REQTEXT.contains(CurrentToken.get().filterNot(_.isWhitespace))

    if (NOERROR){
      u.setName(getTextUntilStopCondition(REQTEXT, List(BRACKETE))
        //filter any spaces from the variable name
        .filterNot(_.isWhitespace))
    }
    else {
      standardSyntaxError("Variable name")
    }

    NOERROR = CurrentToken.get().equalsIgnoreCase(BRACKETE)

    if (NOERROR) {
      ParseTree.add(u)
      //We ended on a bracket, so move past it.
      scanner.getNextToken()
    }
    else {
      standardSyntaxError(BRACKETE)
    }

    return NOERROR
  }


  /* ******************************************************************************************************************
  Document block functions with recursive calls. (These are made up entirely of the non-recursive functions' outputs.)
  I know the mess of if-elses here is not very scala-like, but a match statement was actually messier looking.
  ****************************************************************************************************************** */

  /**
    * The recursive syntax rule for the body (everything between the title and the end of the document).
    *
    * body ::=  inner-text body |
    *           paragraph body  |
    *           newline body    |
    *           ε
    * @return error state (true for no errors)
    */
  def body() : Boolean = {

    //Epsilon (I can't imagine how this might happen)
    if (CurrentToken.get().isEmpty())
    {
      return NOERROR
    }

    if (INNERTEXT.contains(CurrentToken.get().toUpperCase())) {
      NOERROR = innerText()
      return body()
    }

    else if (CurrentToken.get().equalsIgnoreCase(PARAB))
      {
        NOERROR = paragraph()
        return body()
      }

    //Newline is part of inner-text. This isn't needed.
    //else if (CurrentToken.equals(NEWLINE))
    //  {
    //    NOERROR = body()
    //  }
    return NOERROR
  }

  def innerItem() : Boolean = {
    //Case: variable-use
    if (CurrentToken.get().equals(USEB)) {
      NOERROR = variableUse()
      if (NOERROR) return innerItem()
    }

    //Case: Bold
    else if (CurrentToken.get().equals(BOLD)) {
      NOERROR = bold()
      if (NOERROR) {
        return innerItem()
      }
    }

    //Case: link
    else if (CurrentToken.get().equals(LINKB)) {
      NOERROR = link()
      if (NOERROR) {
        return innerItem()
      }
    }

    //Case: plain text. Gather text until we encounter a tag of any sort.
    //This could use the getTextWithVarsUntilStopCondition function instead, but
    //the innerText function already checks if something's variable use, so this is
    //just simpler.
    else if (REQTEXT.contains(CurrentToken.get())) {
      ParseTree.add(getTextUntilStopCondition(REQTEXT, TAGS))
      if (NOERROR) {
        return innerItem()
      }
    }
    return NOERROR
  }

  /**
    * The recursive syntax rule for the text appearing inside the body and paragraph blocks of the document.
    *
    * inner-text: variable-use inner-text |
    *             heading inner-text      |
    *             bold inner-text         |
    *             listitem inner-text     |
    *             image inner-text        |
    *             link inner-text         |
    *             TEXT inner-text         |
    *             ε
    *
    * Variable use, bold, link, and text are part of innerItem().
    *
    * @return error state (true for no errors)
    */
  def innerText() : Boolean = {

    //Epsilon (I can't imagine how this might happen)
    if (CurrentToken.get().isEmpty()) {
        return NOERROR
    }

    //case: Inner Item
    else if (INNERITEM.contains(CurrentToken.get())) {
      NOERROR = innerItem()
      if (NOERROR) {
        return innerText()
      }
    }

    else if (TEXT.contains(CurrentToken.get())) {
        ParseTree.add(getTextUntilStopCondition(TEXT, TAGS))
        if (NOERROR) {
          return innerText()
        }
      }

    //Case: heading
    else if (CurrentToken.get().equals(HEADING)) {
      NOERROR = heading()
      if (NOERROR) {
        return innerText()
      }
    }

    //Case: listitem
    else if (CurrentToken.get().equals(LISTITEM)){
      NOERROR = listItem()
      if (NOERROR) {
        return innerText()
      }
    }

    //Case: image
    else if (CurrentToken.get().equals(IMAGEB)){
      NOERROR = image()
      if (NOERROR) {return innerText()}
    }

    //Case:
    else if (CurrentToken.get().equals(NEWLINE)) {
      newline()
      if (NOERROR) return innerText()
    }

    //A variable definition here is a syntax error. Push past the full def block and then continue.
    else if (CurrentToken.get().equalsIgnoreCase(DEFB))
      {
        standardSyntaxError(DEFB)
        getTextUntilStopCondition(TERMINAL, List(BRACKETE))
        scanner.getNextToken()
        return innerText()
      }

    return NOERROR
  }

  /* ******************************************************************************************************************
  Helper functions.
  ****************************************************************************************************************** */

  /**
    * A helper method to get a string of text in a known list until either an error is reached or a stop
    * condition is satisfied. It will return everything starting with the current token, so must take some
    * care NOT to add e.g. a tag (that is, advance past the tag before calling this function).
    *
    * You can specify a specific end condition, in which case it will stop when that condition is reached OR the
    * token isn't among the acceptable strings.
    *
    * @param acceptableStrings A list of strings; any string not in this list will trigger the error state (after which the loop will also stop).
    * @param stopCondition The list of characters that can trigger the end of the loop.
    * @return The string that's been built from the allowed text.
    */
  def getTextUntilStopCondition(acceptableStrings : List[String], stopCondition : List[String]) : String = {
    var str : String = ""
    if (!stopCondition.contains(CurrentToken.get())) {
      str = str + CurrentToken.get()
    }
    while(!stopCondition.contains(scanner.getNextToken().toUpperCase()) && acceptableStrings.contains(CurrentToken.get().toUpperCase()))
    {
      str = str + CurrentToken.get()
    }
    return str
  }

  def clearWhiteSpace(): Unit = {
    //Clear out optional white space.
    while (WHITESPACE.contains(CurrentToken.get())){
      ParseTree.add(CurrentToken.get())
      scanner.getNextToken()
    }
  }

  /**
    * A helper method to add text in a known list ** or a variable usage ** until either an error is
    * reached or a stop condition is satisfied. It will return everything starting with the current token, so must take some
    * care NOT to add e.g. a tag (that is, advance past the tag before calling this function).
    *
    * You can specify a specific end condition, in which case it will stop when that condition is reached OR the
    * token isn't among the acceptable strings.
    *
    *
    * @param acceptableStrings A list of strings; any string not in this list will trigger the error state (after which the loop will also stop).
    * @param stopCondition The list of characters that can trigger the end of the loop.
    * @return The error state.
    */

  def addTextUntilStopCondition(acceptableStrings : List[String], stopCondition : List[String]): Boolean = {
    while (NOERROR && !stopCondition.contains(CurrentToken.get())){
      if (CurrentToken.get().isEmpty()){
        return NOERROR
      }
      NOERROR = acceptableStrings.contains(CurrentToken.get())
      if (NOERROR) ParseTree.add(getTextUntilStopCondition(acceptableStrings, stopCondition))
      else {
        standardSyntaxError("Member of acceptable tokens " + acceptableStrings.toString())
        scanner.getNextToken()
      }
    }
    return NOERROR
  }

  /* ******************************************************************************************************************
  Error state handling.
  ****************************************************************************************************************** */

  def standardSyntaxError(tag : String): Unit = {
    if (tag.equalsIgnoreCase(DEFB) || CurrentToken.get().equalsIgnoreCase(DEFB)){
      error("Syntax error at line " + LINECOUNT + ": " + DEFB + " not allowed here.")
    }
    else{
      error("Syntax error at line " + LINECOUNT + ", position " + POSITION +  ": " + tag + " expected, " + CurrentToken.get() + " found.")
    }
  }
}