package gtxcompilerproject

/**
  * A class to indicate the end of a bold tag and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

class BoldE {
  override def toString() : String = "</b>"
}
