package gtxcompilerproject

/**
  * An object to hold the current token for analysis by various classes.
  *
  * Author: Jon S. Patton
  */

object CurrentToken {
  private var curToken : String = ""

  /**
    * Adds the provided character to the current token string. Add is overloaded so it can accept
    * * single characters or strings, though add(Char) is meant to be used primarily.
    *
    * @param c
    */
  def add(c: Char): Unit =
  {
    curToken = curToken + c
  }

  /**
    * Adds the provided string to the current token string. Add is overloaded so it can accept
    * single characters or strings, though add(Char) is meant to be used primarily.
    * @param c
    */
  def add(s: String): Unit =
  {
    curToken = curToken + s
  }

  /**
    * Sets the current token back to an empty string.
    */
  def reset() = curToken = ""


  /**
    * A getter method for the currentToken string (which is private)
    * @return The current token string stored by This
    */
  def get() : String = return curToken

  /**
    * Directly sets the current token string to the provided string. This is for testing.
    * @param s
    */
  def set(s: String) = curToken = s
}
