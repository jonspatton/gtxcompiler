package gtxcompilerproject

/**
  * A class to store information for an image and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

case class Image() {

  var address : String = ""
  var alt : String = ""

  /**
    * @return The alt text for an image (required)
    */
  def getAlt() = alt

  /**
    * The web address of the image.
    * @return
    */
  def getAddress() = address

  /**
    * @param s The new alt text.
    */
  def setAlt(s: String) = alt = s

  /**
    * @param s The new address text.
    */
  def setAddress (s : String) = address = s

  /**
    * @param s The string to add to the alt text.
    */
  def addToAlt(s : String) = alt = alt + s

  /**
    * @param s The string to add to the address text.
    */
  def addToAddress(s : String) = address = address + s

  override def toString() : String = "<img src=\"" + address + "\" " + "alt=\"" + alt + "\">"
}
