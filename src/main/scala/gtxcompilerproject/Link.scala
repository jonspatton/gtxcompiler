package gtxcompilerproject

/**
  * A class to store a link's information and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

case class Link() {
  var text : String = ""
  var address : String = ""

  /**
    * @return The web address.
    */
  def getAddress() = address

  /**
    * @return The hypertext to display.
    */
  def getText() = text

  /**
    * @param s The new web address.
    */
  def setAddress(s: String) = address = s

  /**
    * @param s The new hypertext.
    */
  def setText (s : String) = text = s

  /**
    * @param s The string to add to the web address
    */
  def addToAddress(s : String) = address = address + s

  /**
    * @param s The string to add to the hypertext.
    */
  def addToText(s : String) = text = text + s

  override def toString() : String =  "<a href=\"" + address + "\">" + text + "</a>"
}
