package gtxcompilerproject

import scala.io.Source
import java.io._

/**
  * Implementation of the Lexical Analyzer trait; gets tokens,
  * puts them in the nexttoken bin, and reports lexical errors
  *
  * Author: Jon S. Patton
  */
class GtxLexicalAnalyzer (val f : File) extends LexicalAnalyzer {

  val s : Source = Source.fromFile(f.getAbsolutePath)
  var line : Int = 0

  /**
    * Add a character to the string provided
    * @param c The new character
    * @param t The existing string
    * @return t + c
    */
  def addChar(c : Char, t: String) : String = {
    return t + c
  }

  /**
    * Get the next character from the scanner.
    * @return The next character.
    */
  def getChar() : Char = {
    return s.next()
  }

  /**
    * Build a token from characters read in by getChar().
    * @return The string, which may be as short as a single character or as long as the global maximum stored at the
    *         project level.
    */
  def getNextToken() : String = {
    //Reset the current token each time we call this method.
    CurrentToken.reset()

    while (s.hasNext && NOERROR)
      {
        var c = getChar()
        CurrentToken.add(c)
        POSITION = POSITION + 1

        if (CARRIAGERETURN.contains(c.toString())) {
          LINECOUNT = LINECOUNT + 1
          POSITION = 1
        }

        if (!LEGALCHARACTERS.contains(c) || CurrentToken.get().length > MAXTOKENLENGTH) {
          lexicalError()
          CurrentToken.reset()
        }

        //Lookup the token and return it if it's valid.
        else if (lookup(CurrentToken.get()))
          {
            return CurrentToken.get()
          }
      }
    return CurrentToken.get()
  }

  /**
    * Examines the global list of acceptable tokens and reports if the candidate is a valid token.
    * @param candidate The potential token.
    * @return True if valid.
    */
  def lookup(candidate : String) : Boolean = {
    return TERMINAL.contains(candidate.toUpperCase())
  }

  /**
    * Generates a formatted error message specific to lexical errors.
    */
  def lexicalError(): Unit = {
    error("Lexical error at line " + LINECOUNT + ", position " + POSITION + ": " + CurrentToken.get() + " is not a valid token.")
  }
}
