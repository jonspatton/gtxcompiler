package gtxcompilerproject

/**
  * Converts a parse tree of GitTex code into a single string of html. The html can then be
  * saved to a file by client code and opened in a browser.
  * This simply uses the values of constants (stored at the package level) or the class functions
  * of certain parts of the document that can be stored as discrete (almost terminal) blocks
  * that can then report their proper html representation. (See e.g. the Title object). Thus if the html
  * ever changes, the object or constant can be updated and this analyzer need never be opened.
  *
  * Credit for the idea to use objects for parts of the GitTex doc to Dr. Dehlinger.
  *
  * @param parseTree The completed, syntatically correct parse tree containing all valid lexemes or objects
  *                  containing valid lexemes.
  *
  * author: @Jon S. Patton
  */

class GtxSemanticAnalyzer (parseTree: List[Any]) {

  var html : String = ""
  var variablesStack : List[Any] = List()

  /* ******************************************************************************************************************
   Tag processing
   ****************************************************************************************************************** */

  /**
    * Generates or requests from an object stored in the list the html representation of the list item.
    * @param head The head of a list.
    * @return The HTML code for the head.
    */
  def produceTag(head : Any) : String = {
    head match {
      //For variable definitions, add them to a stack that keeps track of the variable definitions
      //in scope.
      case vdef : VariableDefine => {
        variablesStack = vdef :: variablesStack
        return ""
      }
        //When we use a variable, we pop things off the definitions stack until we find a definition
        //with the same name, and replace the VariableUse object with the definition's string.
      case vuse : VariableUse => {
        return getVariableDefInScope(vuse, variablesStack, true)
      }
      case p : ParaB => {
        beginNewScope()
        return p.toString()
      }
      case p : ParaE => {
        endScope()
        return p.toString()
      }
      // These are all of the little document part objects like Title, Link, etc.
      case _ => head.toString()
      }
    }

  /**
    * Recursively shrink the parse tree so that it can be converted to a single string of HTML markup.
    */
  def translateToHTML() = {
    for (leaf <- ParseTree.getReverse()) html = html + produceTag(leaf)
  }

  /**
    * @return The HTML string built by this class.
    */
  def getHTML() : String = html

  /* ******************************************************************************************************************
  Variable processing methods.
  ****************************************************************************************************************** */
  /**
    * If we hit a begin paragraph tag, the variables contained in it will have a scope limited to that paragraph.
    * This is checked later by the
    */
  def beginNewScope(): Unit = {
    variablesStack = PARAB :: variablesStack
  }

  def endScope(): Unit = {
    if (variablesStack == Nil) return
    if (variablesStack(0).equals(PARAB)) {
      variablesStack = variablesStack.tail
    }
    else {
      variablesStack = variablesStack.tail
      endScope()
    }
  }

  /**
    * Examine the stack of variable definitions in scope to locate the appropriate definition of a variable name.
    * @param vuse The variable name being used.
    * @param vstack The stack of variables and scope identifiers
    * @param inScope Whether we are currently in a possible scope fot this variable.
    * @return The variable value.
    */
  def getVariableDefInScope(vuse: VariableUse, vstack : List[Any], inScope : Boolean): String = {

    if (vstack == Nil) {
      semanticError(vuse.getName())
      return ""
    }

    var head = vstack.head
    var retVal = ""

    head match {
        case h : VariableDefine => {
          if (inScope && h.getName().equals(vuse.getName())){
            retVal = h.getValue()
          }
          else {
            retVal = getVariableDefInScope(vuse, vstack.tail, inScope)
          }
        }
        case PARAB => {
          retVal = getVariableDefInScope(vuse, vstack.tail, true)
        }
        case PARAE => {
          retVal = getVariableDefInScope(vuse, vstack.tail, false)
        }
        case _ => retVal = getVariableDefInScope(vuse, vstack.tail, inScope)
    }
    return retVal
  }

  /**
    * Formats a string for semantic errors. At present it only works for variable mistakes because there are no other
    * relevant semantic errors.
    *
    * @param s The variable whose definition was not found.
    */
  def semanticError(s : String) = {
    error("Semantic Error at line " + LINECOUNT + ", position " + POSITION + ": Variable Definition not found for " + s)
    NOERROR = false
  }
}

