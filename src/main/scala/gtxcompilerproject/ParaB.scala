package gtxcompilerproject

/**
  * A class to indicate the start of a paragraph and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

case class ParaB() {
  override def toString() : String = "<p>"

}
