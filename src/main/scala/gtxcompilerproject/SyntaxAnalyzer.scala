package gtxcompilerproject

/**
  * Syntactic analyzer trait to force specific method names on the project (to aid troubleshooting).
  *
  * Author: Dr. J. Dehlinger
  */
trait SyntaxAnalyzer {
  def gittex() : Boolean
  def title() : Boolean
  def body() : Boolean
  def paragraph() : Boolean
  def heading() : Boolean
  def variableDefine() : Boolean
  def variableUse() : Boolean
  def bold() : Boolean
  def listItem() : Boolean
  def link() : Boolean
  def image() : Boolean
  def newline() : Boolean
}
