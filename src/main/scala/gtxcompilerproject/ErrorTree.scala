package gtxcompilerproject

/**
  * An object to store the various error strings produced by the analyzers. The tree is stored with the most recent
  * addition as the head of a list, so to report the errors in order from the top down use the reverse method.
  *
  * Author: Jon S. Patton
  */

object ErrorTree {
  var errorTree : List[Any] = List()

  /**
    * Add an error to the list.
    * @param s The error message.
    */
  def add(s : Any): Unit =
  {
    errorTree = s :: errorTree
  }

  /**
    * @return The reversed list stored by This.
    */
  def getReverse(): List[Any] = {
    return errorTree.reverse
  }

  /**
    * @return The list stored by This.
    */
  def get() : List[Any] = return errorTree
}
