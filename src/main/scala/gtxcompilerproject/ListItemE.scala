package gtxcompilerproject

/**
  * A class to indicate the end of a listitem and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

case class ListItemE() {
  override def toString() : String = "</li>"

}
