package gtxcompilerproject

/**
  * A class to indicate the end of a paragraph and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

case class ParaE() {
  override def toString() : String = "</p>"
}
