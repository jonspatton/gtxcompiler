package gtxcompilerproject

/**
  * A class to indicate the start of a heading and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

case class HeadingB() {

  override def toString() : String = "<h1>"

}
