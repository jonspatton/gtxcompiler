package gtxcompilerproject
/**
  * A class to indicate the start of a document and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */
case class DocB() {
  override def toString() : String = "<html>"
}
