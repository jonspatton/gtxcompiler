package gtxcompilerproject

/**
  * A class to indicate the end of a heading and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

case class HeadingE() {
  override def toString() : String = "</h1>"
}
