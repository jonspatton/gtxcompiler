package gtxcompilerproject

import java.io._
import java.awt.Desktop
import java.nio.file.{Path, Paths}

import scala.io.Source

/**
  * Main driver and pre- and postprocessing. Takes a .gtx input file, creates the parser and scanner, provides the input
  * file scanner to the Syntax and Lexical analyzers, and provides the parse tree to the web browser, or prints the error
  * tree if the file contained errors.
  *
  * author: Jon S. Patton
  *
  * Function to open html in web browser by Dr. J. Dehlinger
  *
  */

object Compiler {

  def main(args: Array[String]): Unit = {

    //Check arguments
    if (args.length == 0)
      {
        //Usage error: Print an error message and quit.
        System.exit(1)
      }

    //Want to be sure that they've actually given us a file.
    var f: File = new File(args(0))

    if (!f.isFile())
      {
        println("File " + args(0) + " does not exist.")
        System.exit(1)
      }

    var fname : String = f.getName()
    //Exit if file extension is wrong
    if (!isGTXFileNameExtension(fname))
      {
        //Usage error
        println("Error: Incorrect file extension.")
        System.exit(1)
      }

    val scanner = new GtxLexicalAnalyzer(f)
    val parser = new GtxSyntaxAnalyzer(scanner)
    var semantic : GtxSemanticAnalyzer = new GtxSemanticAnalyzer(Nil)

    //call start state
    if(parser.gittex() && ErrorTree.get() == Nil)
      {
        //We've built a syntactically valid parse tree.
        semantic = new GtxSemanticAnalyzer(ParseTree.getReverse())
        semantic.translateToHTML()
      }
    if (ErrorTree.get() == Nil)
      {
        val savepath = Paths.get(f.getAbsolutePath).getParent.toString() + "/"
        val outfname = fname.replaceFirst(".gtx", ".html")
        val outfile : File = new File(savepath +  outfname)
        val ps : PrintStream = new PrintStream(outfile)
        ps.print(semantic.getHTML())
        openHTMLFileInBrowser(outfile.getAbsolutePath())
        ps.flush()
        ps.close()
      }
    else {
      ErrorTree.getReverse().foreach((println(_)))
    }

    //Post-processing steps
    //Pass the parse tree to the semantic analyzer
    //Save string to html file
    //Open the html file with
  }

  /**
    * Check that the file extension is valid.
    * @param filename The name of the file to check.
    * @return
    */
  def isGTXFileNameExtension(filename: String): Boolean = filename.endsWith(".gtx")

  /**
    * Scala/Java function to take a String filename and open in default web browswer.
    * Author: Dr. J. Dehlinger
    */
  def openHTMLFileInBrowser(htmlFileStr : String) = {
    val file : File = new File(htmlFileStr.trim)
    if (!file.exists())
      sys.error("File " + htmlFileStr + " does not exist.")

    try {
      Desktop.getDesktop.browse(file.toURI)
    }
    catch {
      case ioe: IOException => sys.error("Failed to open file:  " + htmlFileStr)
      case e: Exception => sys.error("He's dead, Jim!")
    }
  }

}
