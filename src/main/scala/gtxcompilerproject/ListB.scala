package gtxcompilerproject

/**
  * A class to indicate the start of an unordered list and report the relevant HTML code.
  *
  * Author: Jon S. Patton
  */

case class ListB() {

  override def toString() = "<ul>"

}
