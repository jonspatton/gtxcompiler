grammar gtxtext;


/* TERMINAL SYMBOLS / LEXICAL TOKENS */

//Normal text. Separated NEWLINE out since headers and such don't permit them.
TEXT 		:	'0' .. '9' | 'A' .. 'Z' | 'a' .. 'z'| '\'' | '.' | ';' | ':' | ',' | '?' | '_' | '/' | '\"' | ' ' ;
TAB		:	'\t';
NEWLINE		:	'\\\\';
CARRIAGERETURN	:	'\r' | '\n'| '\u000C';

//Tags

// <html> ... </html>
BEGIN		:	'\\BEGIN';
END		:	'\\END';

//<b>, </b>
//BOLD		:	'*';

//<head><title>...</title></head>
//TITLE		:	'\\TITLE';


//<h1> ... </h1>
//HEADING		:	'#';

//<p> ... </p>
//PARB		:	'\\PARB';
//PARE		:	'\\PARE';

//<ul><li>..</li> .. </ul>
//LIST		:	'+';

//<img src=" ... ">...</img>
//IMAGE		:	!;

//Variable Definition and usage tag
//VARDEF		:	'\\DEF';
//USEDEF		:	'\\USE';

document	:	BEGIN text END;
text		:	TEXT* TAB* NEWLINE* CARRIAGERETURN*;