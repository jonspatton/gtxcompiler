grammar gtxgrammar;

/* TERMINAL SYMBOLS / LEXICAL TOKENS */

//Normal text. Separated NEWLINE out since headers and such don't permit them.
TEXT 		:	'0' .. '9' | 'A' .. 'Z' | 'a' .. 'z'| '.' | ';' | ':' | ',' | '?' | '_' | '/' | '\"';
SPACE		:	' ';
TAB		:	'\t';
NEWLINE		:	'\\\\';
CARRIAGERETURN	:	'\r' | '\n'| '\u000C';

//Tags

// <html> ... </html>
BEGIN		:	'\\' ('B'|'b') ('E'|'e') ('G'|'g') ('I'|'i') ('N'|'n');
END		:	'\\' ('E'|'e') ('N'|'n') ('D'|'d');

//<head><title>...</title></head>
TITLE		:	'\\' ('T'|'t') ('I'|'i') ('T'|'t') ('L'|'l') ('E'|'e');

//<p> ... </p>
PARB		:	'\\'('P'|'p') ('A'|'a') ('R'|'r') ('B'|'b');
PARE		:	'\\'('P'|'p') ('A'|'a') ('R'|'r') ('E'|'e');

//Variable Definition and usage tag
VARDEF		:	'\\' ('D'|'d') ('E'|'e') ('F'|'f');
USEDEF		:	'\\' ('U'|'u') ('S'|'s') ('E'|'e');

/* SYNTAX */
//Document order with title before vardef is from the example including a vardef, which contradicts the description of variable definitions
document	:	BEGIN whitespace* title whitespace* vardef* body* END whitespace*;

//this is less readable from the interpreter, but it keeps this from freaking out if there's an errant space, tab, or hard return
whitespace	:	CARRIAGERETURN | TAB | SPACE;
text		:	TEXT | NEWLINE | whitespace;
//[] and () with text.
bracketopttext	:	'[' (TEXT|SPACE|usedef)* ']';
bracketreqtext	:	'[' (TEXT|SPACE|usedef)+ ']';
parenreqtext	:	'(' (TEXT|SPACE|usedef)+ ')';

//The meat and potatoes
body		:	paragraph | heading | list | text | bold | link | image | usedef;

//Blocks that include text.
bold		:	'*' (text|usedef)* '*';
title		:	TITLE bracketopttext CARRIAGERETURN;

//(TEXT|SPACE|usedef)+ should use * but there's a bug in the interpreter that won't recognize if there's a space first (gives a mismatched token error)
//In other words, it won't treat (TEXT|SPACE|usedef)* the same as (SPACE|TEXT|usedef)*
heading		:	'# ' (TEXT|SPACE|usedef)+ CARRIAGERETURN;
paragraph	:	PARB vardef* (text | bold | link | usedef)* PARE;
list		:	'+' (TEXT | bold | link | usedef | SPACE)* CARRIAGERETURN;

//<a href=" ..."> .. </a>

link		:	bracketreqtext parenreqtext;
//This is translated differently but it's actually the same as ! followed by a link.
image		:	'!' link;


//\DEF[variable name  = value], with variable name, =, and value required.
vardef		:	VARDEF '[' SPACE* (TEXT|usedef)+ SPACE* '=' SPACE* (TEXT|usedef)+ SPACE* ']';
usedef		: 	USEDEF bracketreqtext;