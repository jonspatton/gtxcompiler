// $ANTLR 3.3 Nov 30, 2010 12:45:30 /Users/admin/Documents/COSC 455/gtxcompiler/grammar/gtxgrammar.g 2018-10-06 12:23:14

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class gtxgrammarLexer extends Lexer {
    public static final int EOF=-1;
    public static final int TEXT=4;

    // delegates
    // delegators

    public gtxgrammarLexer() {;} 
    public gtxgrammarLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public gtxgrammarLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "/Users/admin/Documents/COSC 455/gtxcompiler/grammar/gtxgrammar.g"; }

    // $ANTLR start "TEXT"
    public final void mTEXT() throws RecognitionException {
        try {
            int _type = TEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/admin/Documents/COSC 455/gtxcompiler/grammar/gtxgrammar.g:4:8: ( '\\\"' )
            // /Users/admin/Documents/COSC 455/gtxcompiler/grammar/gtxgrammar.g:4:10: '\\\"'
            {
            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "TEXT"

    public void mTokens() throws RecognitionException {
        // /Users/admin/Documents/COSC 455/gtxcompiler/grammar/gtxgrammar.g:1:8: ( TEXT )
        // /Users/admin/Documents/COSC 455/gtxcompiler/grammar/gtxgrammar.g:1:10: TEXT
        {
        mTEXT(); 

        }


    }


 

}