// $ANTLR 3.3 Nov 30, 2010 12:45:30 /Users/admin/Documents/COSC 455/gtxcompiler/grammar/gtxgrammar.g 2018-10-06 12:23:14

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.debug.*;
import java.io.IOException;
public class gtxgrammarParser extends DebugParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "TEXT"
    };
    public static final int EOF=-1;
    public static final int TEXT=4;

    // delegates
    // delegators

    public static final String[] ruleNames = new String[] {
        "invalidRule", 
    };
    public static final boolean[] decisionCanBacktrack = new boolean[] {
        false, // invalid decision
    };

     
        public int ruleLevel = 0;
        public int getRuleLevel() { return ruleLevel; }
        public void incRuleLevel() { ruleLevel++; }
        public void decRuleLevel() { ruleLevel--; }
        public (TokenStream input) {
            this(input, DebugEventSocketProxy.DEFAULT_DEBUGGER_PORT, new RecognizerSharedState());
        }
        public (TokenStream input, int port, RecognizerSharedState state) {
            super(input, state);
            DebugEventSocketProxy proxy =
                new DebugEventSocketProxy(this, port, null);
            setDebugListener(proxy);
            try {
                proxy.handshake();
            }
            catch (IOException ioe) {
                reportError(ioe);
            }
        }
    public (TokenStream input, DebugEventListener dbg) {
        super(input, dbg, new RecognizerSharedState());

    }
    protected boolean evalPredicate(boolean result, String predicate) {
        dbg.semanticPredicate(result, predicate);
        return result;
    }


    public String[] getTokenNames() { return gtxgrammarParser.tokenNames; }
    public String getGrammarFileName() { return "/Users/admin/Documents/COSC 455/gtxcompiler/grammar/gtxgrammar.g"; }



    // Delegated rules


 

}