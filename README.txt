A compiler for the madeup "GitTex" language that is partially git markup and partially latex code.

Takes an input file as the command line argument with a .gtx extension and converts it to HTML, which is then opened in the default web browser.

See the BNF grammar for legal codes and characters.

Gitlab repository: https://gitlab.com/jonspatton/gtxcompiler

Scaladoc reference: index.html in the root directory.